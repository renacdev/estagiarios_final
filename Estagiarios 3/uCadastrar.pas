unit uCadastrar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uPrincipal, dxGDIPlusClasses, Mask, uEstagiarios;

type
  TfmCadastrar = class(TForm)
  Panel2: TPanel;
  edNome: TEdit;
  lb2: TLabel;
  lb3: TLabel;
  lb4: TLabel;
  rd1: TRadioButton;
  rd2: TRadioButton;
  rd3: TRadioButton;
  btnEnviar: TButton;
  Image1: TImage;
  Image2: TImage;
  Label1: TLabel;
  btnLimpar: TButton;
  edTelefone: TMaskEdit;
  procedure btnEnviarClick(Sender: TObject);
  procedure btnLimparClick(Sender: TObject);
  procedure edNomeKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
    function getSetor : string;
  public
//  novoCadastro = string;
end;

var
  fmCadastrar: TfmCadastrar;
implementation

uses uListar;

{$R *.dfm}

procedure TfmCadastrar.btnEnviarClick(Sender: TObject);
var
  wl_a: integer;
  wl_pos: Integer;
  wl_est: TEstagiarios;
//Valida��o p/ n�o deixar campos Nome e Telefone em branco
begin
  if edNome.Text = '' then
    begin
      showMessage('Digite um nome');
      edNome.SetFocus;
      exit;
    end;

  if edTelefone.Text = '' then
    begin
      showMessage('Digite seu n�mero de telefone');
      edTelefone.SetFocus;
      exit;
    end;

//**************************************************

// Valida��o p/ n�o deixar setor em branco
  if rd1.checked or rd2.checked or rd3.checked = false then
    begin
      showMessage('Informe o Setor');
      rd1.SetFocus;;
      exit;
    end;
//**********************************************************

  AddEstagiario(edNome.Text, edTelefone.Text, getSetor, (400 + Random(600)));

  showMessage('Usu�rio Cadastrado');
  Close;
end;

procedure TfmCadastrar.btnLimparClick(Sender: TObject);
begin
  edNome.Text:= '';
  edTelefone.Text:='';
  rd1.Checked := false;
  rd2.Checked := false;
  rd3.Checked := false;
end;

procedure TfmCadastrar.edNomeKeyPress(Sender: TObject; var Key: Char);
//Valida��o p/ digitar apenas letras no edNome
begin
  if not (Key in['A'..'Z']) and not (Key in['a'..'z']) then
  Key := #13 ;
end;
//************************************************************

//M�todo p/ receber o setor
function TfmCadastrar.getSetor: string;
begin
  if rd1.checked = true then
    result:= rd1.caption
  else if rd2.checked = true then
    result:= rd2.caption
  else if rd3.checked = true then
    result:= rd3.caption;
end;
//***********************************************************
end.
