object fmSalario: TfmSalario
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'M'#233'dia Salarial'
  ClientHeight = 341
  ClientWidth = 574
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 41
    Align = alTop
    Color = 1741567
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 219
      Top = 14
      Width = 167
      Height = 23
      Align = alCustom
      Caption = 'M'#201'DIA SALARIAL'
      Color = clBackground
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object btnMedia: TButton
    Left = 0
    Top = 43
    Width = 574
    Height = 38
    Caption = 'Gerar M'#233'dia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnMediaClick
  end
  object btnListar: TButton
    Left = 0
    Top = 83
    Width = 574
    Height = 38
    Caption = 'Listar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btnListarClick
  end
end
