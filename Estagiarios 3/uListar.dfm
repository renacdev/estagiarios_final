object fmListar: TfmListar
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'fmListar'
  ClientHeight = 341
  ClientWidth = 574
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 57
    Align = alTop
    Color = 1741567
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 296
      Top = 16
      Width = 3
      Height = 13
    end
    object btnListar: TButton
      Left = 227
      Top = 13
      Width = 137
      Height = 34
      Caption = 'Listar'
      TabOrder = 0
      OnClick = btnListarClick
    end
  end
end
