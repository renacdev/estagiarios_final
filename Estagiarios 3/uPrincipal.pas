unit uPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, dxGDIPlusClasses, ExtCtrls,uEstagiarios;

var
  wg_n: integer;
//  novoCadastro: array[0..4] of string;
  lbNome: TLabel;
  lbTelefone: TLabel;
  lbSetor: TLabel;
  lbSalario: TLabel;
//  TLabel: TLabel;

type
  TfmPrincipal = class(TForm)
  btnCriar: TButton;
  btnListar: TButton;
  btnPesquisar: TButton;
  btnMedia: TButton;
  Image1: TImage;
  pnLaranja: TPanel;
  Image2: TImage;
  procedure btnCriarClick(Sender: TObject);
  procedure btnListarClick(Sender: TObject);
  procedure btnPesquisarClick(Sender: TObject);
  procedure btnMediaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
end;

var
  fmPrincipal: TfmPrincipal;

implementation

uses  uCadastrar, uListar, uPesquisar, uSalario;

{$R *.dfm}

procedure TfmPrincipal.btnCriarClick(Sender: TObject);
begin
  fmCadastrar:=TfmCadastrar.Create(self);
  try
    fmCadastrar.ShowModal;
  finally
    fmCadastrar.Free;
  end;
end;

procedure TfmPrincipal.btnListarClick(Sender: TObject);

begin
  fmListar:= TfmListar.Create(self);
  try
    fmListar.ShowModal;
  finally
    fmListar.Free;
  end;
end;

procedure TfmPrincipal.btnMediaClick(Sender: TObject);

begin
  fmSalario:= TfmSalario.Create(self);
  try
    fmSalario.ShowModal;
  finally
    fmSalario.Free;
  end;
end;

procedure TfmPrincipal.btnPesquisarClick(Sender: TObject);
begin
  fmPesquisar:= TfmPesquisar.Create(self);
  try
    fmPesquisar.ShowModal;
  finally
    fmPesquisar.Free;
  end;
end;

procedure TfmPrincipal.FormCreate(Sender: TObject);
begin
  AddEstagiario ('Daniel' , '85 899494944' , 'Implantação' , 800);
  AddEstagiario ('Gustavo' , '85 899494944' , 'Desenvolvimento' , 800);
  AddEstagiario ('Isac' , '85 899494944' , 'Desenvolvimento' , 800);
  AddEstagiario ('Luca' , '85 899494944' , 'Suporte', 800);
  AddEstagiario ('Renan' , '85 899494944' , 'Desenvolvimento' , 800);
  AddEstagiario ('Renato' , '85 899494944' , 'Desenvolvimento' , 800);

end;

end.
